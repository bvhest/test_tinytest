#' @title addOne
#'
#' @description
#' blaah.
#' @details
#' blaah.
#'
#' @return a number
#' @keywords addition
#' @export
addOne <- 
  function(x) x + 2
